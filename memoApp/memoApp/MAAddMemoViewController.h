//
//  MAAddMemoViewController.h
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/13.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MADataManager.h"

@interface MAAddMemoViewController : UIViewController

@property MADataManager* dataManager;

@end
