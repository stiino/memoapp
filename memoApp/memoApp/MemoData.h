//
//  MemoData.h
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/13.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MemoData : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * memoText;

@end
