//
//  MAEditMemoViewContoller.h
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/25.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MADataManager.h"

@interface MAEditMemoViewContoller : UIViewController{
    NSString* _memoTitle;
    NSString* _text;
}

@property (nonatomic) NSString* memoTitle;
@property NSString* text;
@property MADataManager* dataManager;
@property NSInteger num;

@end
