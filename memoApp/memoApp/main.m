//
//  main.m
//  memoApp
//
//  Created by 葛 智紀 on 2014/12/30.
//  Copyright (c) 2014年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAAppDelegate class]));
    }
}
