//
//  MADataManager.h
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/14.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MADataManager : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (BOOL)addMemoTitle:(NSString *)title andMemoText:(NSString *)memoText;
- (NSMutableArray *)fetch:(NSString *)entityName sortKey:(NSString *)key;
-(BOOL)newTitleValue:(NSString *)newTitleValue newTextValue:(NSString *) newTextValue fetchEntity:(NSString*)entityName recordNum:(NSInteger)num;
-(BOOL)deleteEntityName:(NSString *)entityName num:(NSInteger)num;

@end
