//
//  MAAppDelegate.h
//  memoApp
//
//  Created by 葛 智紀 on 2014/12/30.
//  Copyright (c) 2014年 葛 智紀. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

