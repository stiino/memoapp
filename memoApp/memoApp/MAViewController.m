//
//  MAViewController.m
//  memoApp
//
//  Created by 葛 智紀 on 2014/12/30.
//  Copyright (c) 2014年 葛 智紀. All rights reserved.
//

#import "MemoData.h"
#import "MAViewController.h"
#import "MADataManager.h"
#import "MAAddMemoViewController.h"
#import "MAEditMemoViewContoller.h"

@interface MAViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView* memoTableView;
@property (strong, nonatomic) NSMutableArray* memoTitleArray;
@property NSMutableArray* memoTextArray;
@property MADataManager* dataManager;
@property NSIndexPath* index;

@end

@implementation MAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self loadData];
    
    self.memoTableView.delegate = self;
    self.memoTableView.dataSource = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadData];
    
    [_memoTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//データ数を返す
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_memoTitleArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [_memoTitleArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addMemo"]) {
        MAAddMemoViewController* addMemoViewController = [segue destinationViewController];
        addMemoViewController.dataManager = _dataManager;
    }
    else if([[segue identifier] isEqualToString:@"pushCell"]){
        MAEditMemoViewContoller* editMemoViewController = [segue destinationViewController];
        editMemoViewController.memoTitle = [_memoTitleArray objectAtIndex:_index.row];
        editMemoViewController.text = [_memoTextArray objectAtIndex:_index.row];
        editMemoViewController.dataManager = _dataManager;
        editMemoViewController.num = _index.row;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _index = indexPath;
    [self performSegueWithIdentifier:@"pushCell" sender:indexPath];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        BOOL result;
        result = [_dataManager deleteEntityName:@"MemoData" num:indexPath.row];
        if (!result) {
            NSLog(@"delete error");
        }
    }
    
    [self loadData];
    
    [_memoTableView reloadData];
}

-(void)loadData
{
    _dataManager = [[MADataManager alloc]init];
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    
    tmpArray = [_dataManager fetch:@"MemoData" sortKey:@""];
    
    _memoTitleArray = [[NSMutableArray alloc]init];
    _memoTextArray = [[NSMutableArray alloc]init];
    
    for(NSManagedObject *data in tmpArray)
    {
        MemoData* memo = (MemoData *) data;
        NSLog(@"Name = %@", memo.title);
        NSLog(@"Address = %@", memo.memoText);
        [_memoTitleArray addObject:memo.title];
        [_memoTextArray addObject:memo.memoText];
    }
}

@end
