//
//  MAEditMemoViewContoller.m
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/25.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import "MAEditMemoViewContoller.h"

@interface MAEditMemoViewContoller ()
@property (strong, nonatomic) IBOutlet UITextField *memoTitleField;
@property (strong, nonatomic) IBOutlet UITextView *memoTextView;

@end

@implementation MAEditMemoViewContoller
@synthesize memoTitle = _memoTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _memoTitleField.text = _memoTitle;
    _memoTextView.text = _text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editButton:(id)sender {
    [_dataManager newTitleValue:_memoTitleField.text newTextValue:_memoTextView.text fetchEntity:@"MemoData" recordNum:_num];
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
