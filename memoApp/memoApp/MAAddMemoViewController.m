//
//  MAAddMemoViewController.m
//  memoApp
//
//  Created by 葛 智紀 on 2015/01/13.
//  Copyright (c) 2015年 葛 智紀. All rights reserved.
//

#import "MAAddMemoViewController.h"

@interface MAAddMemoViewController ()
@property (strong, nonatomic) IBOutlet UITextField *memoTitleField;
@property (strong, nonatomic) IBOutlet UITextView *memoText;

@end

@implementation MAAddMemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveBarButton:(id)sender {
    NSString* memoTitleStr = [_memoTitleField.text mutableCopy];
    NSString* memoTextStr = [_memoText.text mutableCopy];
    [_dataManager addMemoTitle:memoTitleStr andMemoText:memoTextStr];
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
